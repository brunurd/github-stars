# Github Stars
**Por [Bruno Araujo](https://github.com/brunurd)**

---

## Dependências

- [Node.js com npm](https://nodejs.org/en/download/) - versão 5 ou superior.

---

## Como rodar

**1. Instalar todas dependências:**  
Na pasta do projeto execute o comando do npm.
```bash
npm i
```
  
  
**2. Executar projeto:**  
Para executar o projeto basta rodar esse comando na pasta do projeto.
```bash
npm start
```
