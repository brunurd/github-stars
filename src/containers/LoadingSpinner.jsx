import React from 'react'
import { LoadingSpinnerStyle } from '../ui'
import star from '../assets/icons/star-fill.svg'

const LoadingSpinner = () => {
  return (
    <LoadingSpinnerStyle>
      <img src={star} width={46} height={40} />
      <p>Searching...</p>
    </LoadingSpinnerStyle>
  )
}

export default LoadingSpinner