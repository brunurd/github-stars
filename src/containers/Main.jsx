import React from 'react'
import { PropTypes } from 'prop-types'
import { connect, useSelector } from 'react-redux'
import SearchInput from './SearchInput.jsx'
import UserBar from './UserBar.jsx'
import RepoList from './RepoList.jsx'
import Notification from './Notification.jsx'
import { MainContainer, MainStyle } from '../ui'
import { SearchStates } from '../store/reducers/search'
import UnfoundUser from './UnfoundUser.jsx'
import LoadingSpinner from './LoadingSpinner.jsx'
import Modal from './Modal.jsx'

const Main = ({ match, history }) => {
  const searchState = useSelector(state => state.search.searchState)
  const { sentence } = match.params

  const mainContainer = () => {
    switch(searchState) {
    case SearchStates.FOUND:
      return (
        <MainContainer>
          <UserBar />
          <RepoList />
        </MainContainer>
      )
    case SearchStates.SEARCHING:
      return <LoadingSpinner />
    case SearchStates.UNFOUND:
      return <UnfoundUser />
    }
  }

  return (
    <MainStyle>
      <SearchInput history={history} pathSentence={sentence} />
      {mainContainer()}
      <Notification />
      <Modal />
    </MainStyle>
  )
}

Main.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
}

export default connect()(Main)