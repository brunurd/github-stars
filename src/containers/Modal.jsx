import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect, useSelector } from 'react-redux'
import { ModalStyle, Button } from '../ui'
import { hideModal } from '../store/reducers/viewer'
import { addStar } from '../utils/graphql'
import updateViewerData from '../utils/updateViewerData'

const Modal = ({ hideModal }) => {
  const [token, setToken] = useState(useSelector(state => state.viewer.token))
  const viewerLogin = useSelector(state => state.viewer.login)
  const modalIsActive = useSelector(state => state.viewer.modalIsActive)
  const lastRepositoryId = useSelector(state => state.viewer.lastRepositoryId)
  const username = useSelector(state => state.user.login)

  const handleSubmit = event => {
    event.preventDefault()
    return false
  }

  const handleConfirm = () => {
    if (token) {
      updateViewerData(token, username)
    }

    if (lastRepositoryId) {
      addStar(token, viewerLogin, lastRepositoryId)
    }
  }

  return (
    <ModalStyle active={modalIsActive}>
      <form onSubmit={handleSubmit}>
        <h1>Save your token first...</h1>
        <label>To star or unstar a user starred repository you need a github account and a public token with the scope <strong>public_repo</strong> <a href="https://github.com/settings/tokens" rel="noopener noreferrer" target="_blank">generate</a> and paste below:</label>
        <input
          placeholder="Paste your token here..."
          value={token}
          name="token"
          onChange={e => setToken(e.target.value)}
        />
        <nav>
          <Button type="button" onClick={handleConfirm}>Confirm</Button>
          <Button type="button" onClick={hideModal}>Cancel</Button>
        </nav>
      </form>
    </ModalStyle>
  )
}

Modal.propTypes = {
  hideModal: PropTypes.func,
}

export default connect(null, { hideModal })(Modal)