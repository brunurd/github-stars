import React from 'react'
import { PropTypes } from 'prop-types'
import { connect, useSelector } from 'react-redux'
import { NotificationStyle, Button } from '../ui'
import { deactive } from '../store/reducers/notification'

const Notification = ({ deactive }) => {
  const text = useSelector(state => state.notification.text)
  const active = useSelector(state => state.notification.active)

  return (
    <NotificationStyle active={active}>
      <p>{text}</p>
      <Button onClick={deactive}>dismiss</Button>
    </NotificationStyle>
  )
}

Notification.propTypes = {
  deactive: PropTypes.func,
}

export default connect(null, { deactive })(Notification)