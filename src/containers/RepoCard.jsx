import React, { useState } from 'react'
import { connect, useSelector } from 'react-redux'
import { PropTypes } from 'prop-types'
import { RepoCardStyle, Icon, IconTextContainer, Button } from '../ui'
import { showModal } from '../store/reducers/viewer'
import { sendNotification, deactive } from '../store/reducers/notification'
import { addStar, removeStar } from '../utils/graphql'

const RepoCard = ({ deactiveNotification, id, mappedRepository, sendNotification, showModal }) => {
  const token = useSelector(state => state.viewer.token)
  const login = useSelector(state => state.viewer.login)
  const [repository, setRepository] = useState(mappedRepository)

  const handleStarButtonClick = () => {
    if (!token) {
      showModal(repository.id)
      return
    }

    setRepository({ ...repository, viewerHasStarred: !repository.viewerHasStarred })

    const starCallback = message => {
      sendNotification(message)

      setTimeout(() => {
        deactiveNotification()
      }, 5000)
    }

    if (repository.viewerHasStarred) {
      const message = `Repo "${repository.nameWithOwner}" unstarred with success!`
      removeStar(token, login, repository.id, () => starCallback(message))
    }

    else {
      const message = `Repo "${repository.nameWithOwner}" starred with success!`
      addStar(token, login, repository.id, () => starCallback(message))
    }
  }

  return (
    <RepoCardStyle id={id} starred={repository.viewerHasStarred}>
      <a href={repository.url} rel="noopener noreferrer" target="_blank">
        <div>
          <h3>{repository.nameWithOwner}</h3>
          <p>{repository.description}</p>
          <IconTextContainer>
            <Icon asset="icons/star.svg" width={18} height={18} />
            <p>{repository.stargazers.totalCount}</p>
          </IconTextContainer>
        </div>
      </a>
      <Button onClick={handleStarButtonClick}>{repository.viewerHasStarred ? 'unstar' : 'star'}</Button>
    </RepoCardStyle>
  )
}

RepoCard.propTypes = {
  deactiveNotification: PropTypes.func,
  id: PropTypes.string,
  mappedRepository: PropTypes.object,
  sendNotification: PropTypes.func,
  showModal: PropTypes.func,
}

export default connect(null, { deactiveNotification: deactive, sendNotification, showModal })(RepoCard)