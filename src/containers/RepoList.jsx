import React, { useEffect } from 'react'
import { connect, useSelector } from 'react-redux'
import { RepoListStyle } from '../ui'
import RepoCard from './RepoCard.jsx'
import updateStarredRepositories from '../utils/updateStarredRepositories'

const RepoList = () => {
  const username = useSelector(state => state.user.login)
  const repositories = useSelector(state => state.repositories.repositories)
  const lastCursor = useSelector(state => state.repositories.lastCursor)
  const token = useSelector(state => state.viewer.token)

  function updateOnScroll() {
    const page = document.documentElement
    const position = page.scrollTop
    const maxPosition = page.scrollHeight - page.clientHeight - 100

    if (position >= maxPosition) {
      updateStarredRepositories(username, null, lastCursor, token)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', updateOnScroll)

    return () => {
      window.removeEventListener('scroll', updateOnScroll)
    }
  })

  return (
    <RepoListStyle>
      {repositories.map((repository, index) => {
        return (<RepoCard id={repository.id} key={index} mappedRepository={repository} />)
      })}
    </RepoListStyle>
  )
}

export default connect()(RepoList)