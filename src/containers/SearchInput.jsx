import React from 'react'
import { PropTypes } from 'prop-types'
import { connect, useSelector } from 'react-redux'
import { updateSentence, SearchStates } from '../store/reducers/search'
import { FieldSet, SearchInputStyle, RoundedImage, Icon, Button } from '../ui'
import logo from '../assets/logo.svg'
import avatar from '../assets/avatar.svg'
import { showModal } from '../store/reducers/viewer'
import search from '../utils/search'
import updateViewerData from '../utils/updateViewerData'

const SearchInput = ({ history, pathSentence, updateSentence, showModal }) => {
  const sentence = useSelector(state => state.search.sentence)
  const token = useSelector(state => state.viewer.token)
  const searchState = useSelector(state => state.search.searchState)
  const avatarUrl = useSelector(state => state.viewer.avatarUrl)

  if (pathSentence && searchState === SearchStates.NONE) {
    search(pathSentence, token)
  }

  if (token && !avatarUrl) {
    updateViewerData(token)
  }

  const handleSubmit = event => {
    event.preventDefault()
    search(sentence, token)
    history.push(`/${sentence}`)
    return false
  }

  return (
    <SearchInputStyle center={searchState === SearchStates.NONE}>
      <img src={logo} width={187} height={40} />
      <form onSubmit={handleSubmit}>
        <FieldSet>
          <input
            type="text"
            placeholder="github username..."
            value={sentence}
            onChange={e => updateSentence(e.target.value)}
          />
          <Button>
            <Icon asset="icons/search.svg" width={20} height={20} />
          </Button>
        </FieldSet>
      </form>
      <Button onClick={showModal}>
        <RoundedImage src={avatarUrl || avatar} />
      </Button>
    </SearchInputStyle>
  )
}

SearchInput.propTypes = {
  history: PropTypes.object,
  pathSentence: PropTypes.string,
  updateSentence: PropTypes.func,
  showModal: PropTypes.func,
}

export default connect(null, {
  updateSentence,
  showModal,
})(SearchInput)