import React from 'react'
import { UnfoundUserStyle } from '../ui'
import sadFace from '../assets/icons/sad-face.svg'

const UnfoundUser = () => {
  return (
    <UnfoundUserStyle>
      <img src={sadFace} width={92} height={81} />
      <p>User not found</p>
    </UnfoundUserStyle>
  )
}

export default UnfoundUser