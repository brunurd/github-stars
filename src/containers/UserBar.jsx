import React from 'react'
import { connect, useSelector } from 'react-redux'
import { UserBarStyle, UserBarCard, UserBarInfo, RoundedImage, Icon, IconTextContainer } from '../ui'
import avatar from '../assets/avatar.svg'

const UserBar = () => {
  const user = useSelector(state => state.user)

  function renderInfo(prop, asset, link = null) {
    if (!prop) {
      return null
    }

    let text = <p>{prop}</p>

    if (link) {
      text = (<a href={link} rel="noopener noreferrer" target="_blank">
        <p>{prop}</p>
      </a>)
    }

    return (
      <IconTextContainer>
        <Icon asset={asset} width={18} height={18} />
        {text}
      </IconTextContainer>
    )
  }

  function mountGithubUrl(path) {
    if (!path) {
      return null
    }

    if (path[0] === '@') {
      path = path.substring(1)
    }

    return `https://github.com/${path}`
  }

  function mountUrl(url) {
    if (!url) {
      return null
    }

    if (!url.includes('http://') && !url.includes('https://')) {
      return `http://${url}`
    }

    return url
  }

  return (
    <UserBarStyle>
      <UserBarCard>
        <a href={mountGithubUrl(user.login)} rel="noopener noreferrer" target="_blank">
          <RoundedImage src={user.avatarUrl || avatar} />
          <h1>{user.name}</h1>
          <h2>{user.login}</h2>
        </a>
      </UserBarCard>
      <UserBarInfo>
        <p>{user.bio}</p>
        {renderInfo(user.company, 'icons/users.svg', mountGithubUrl(user.company))}
        {renderInfo(user.location, 'icons/map-pin.svg')}
        {renderInfo(user.email, 'icons/mail.svg', `mailto:${user.email}`)}
        {renderInfo(user.websiteUrl, 'icons/globe.svg', mountUrl(user.websiteUrl))}
      </UserBarInfo>
    </UserBarStyle>
  )
}

UserBar.propTypes = {}

export default connect()(UserBar)