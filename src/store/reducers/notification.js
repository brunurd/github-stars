const initialState = {
  text: '',
  active: false,
}

export const Actions = {
  SEND: 'notification/SEND',
  DEACTIVE: 'notification/DEACTIVE',
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
  case Actions.SEND:
    return { text: action.payload, active: true }
  case Actions.DEACTIVE:
    return { ...state, active: false }
  default:
    return state
  }
}

export function sendNotification(notification) {
  return {
    type: Actions.SEND,
    payload: notification,
  }
}

export function deactive() {
  return {
    type: Actions.DEACTIVE,
  }
}