const initialState = {
  lastCursor: null,
  repositories: [],
}

export const Actions = {
  SET_LAST_CURSOR: 'stars/SET_LAST_CURSOR',
  ADD_REPOSITORY: 'stars/ADD_REPOSITORY',
  RESET: 'stars/RESET',
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
  case Actions.SET_LAST_CURSOR:
    return { ...state, lastCursor: action.payload }
  case Actions.ADD_REPOSITORY:
    for (let repo of state.repositories) {
      if (repo.id === action.payload.id) {
        return { ...state }
      }
    }

    state.repositories.push({
      ...action.payload, 
      viewerHasStarred: action.payload.viewerHasStarred || false,
    })

    return { ...state }
  case Actions.RESET:
    return {
      lastCursor: null,
      repositories: [],
    }
  default:
    return state
  }
}

export function setLastCursor(lastCursor) {
  return {
    type: Actions.SET_LAST_CURSOR,
    payload: lastCursor,
  }
}

export function addRepository(repository) {
  return {
    type: Actions.ADD_REPOSITORY,
    payload: repository,
  }
}

export function reset() {
  return {
    type: Actions.RESET,
  }
}