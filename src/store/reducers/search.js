export const SearchStates = {
  NONE: 'None',
  SEARCHING: 'Searching',
  UNFOUND: 'Unfound',
  FOUND: 'Found',
}

const initialState = {
  sentence: '',
  searchState: SearchStates.NONE,
}

export const Actions = {
  UPDATE_SENTENCE: 'search/UPDATE_SENTENCE',
  SEARCH_SENTENCE: 'search/SEARCH_SENTENCE',
  SET_STATE: 'search/SET_STATE',
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
  case Actions.UPDATE_SENTENCE:
    return { ...state, sentence: action.payload }
  case Actions.SEARCH_SENTENCE:
    return { 
      sentence: action.payload, 
      searchState: SearchStates.SEARCHING,
    }
  case Actions.SET_STATE:
    return { ...state, searchState: action.payload }
  default:
    return state
  }
}

export function updateSentence(sentence) {
  return {
    type: Actions.UPDATE_SENTENCE,
    payload: sentence,
  }
}

export function searchSentence(sentence) {
  return {
    type: Actions.SEARCH_SENTENCE,
    payload: sentence,
  }
}

export function setSearchState(state) {
  return {
    type: Actions.SET_STATE,
    payload: Object.values(SearchStates).includes(state) ? state : SearchStates.NONE,
  }
}