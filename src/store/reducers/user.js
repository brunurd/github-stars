const initialState = {
  login: '',
  name: '',
  bio: '',
  location: '',
  company: '',
  email: '',
  websiteUrl: '',
  avatarUrl: '',
}

export const Actions = {
  UPDATE_USER: 'user/UPDATE_USER',
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
  case Actions.UPDATE_USER:
    return { ...state, ...action.payload }
  default:
    return state
  }
}

export function updateUser(user) {
  return {
    type: Actions.UPDATE_USER,
    payload: user,
  }
}