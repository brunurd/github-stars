import { getCookie, setCookie } from '../../utils/cookie'

const initialState = {
  token: getCookie('userToken'),
  login: '',
  avatarUrl: '',
  lastRepositoryId: '',
  modalIsActive: false,
}

export const Actions = {
  CONFIRM_TOKEN: 'token/CONFIRM_TOKEN',
  SET_VIEWER_DATA: 'token/SET_VIEWER_DATA',
  SHOW_MODAL: 'token/SHOW_MODAL',
  HIDE_MODAL: 'token/HIDE_MODAL',
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
  case Actions.CONFIRM_TOKEN:
    setCookie('userToken', action.payload)
    return { ...state, token: action.payload, modalIsActive: false }
  case Actions.SET_VIEWER_DATA:
    return { ...state, ...action.payload }
  case Actions.SHOW_MODAL:
    return { ...state, modalIsActive: true, lastRepositoryId: action.payload }
  case Actions.HIDE_MODAL:
    return { ...state, modalIsActive: false }
  default:
    return state
  }
}

export function confirmToken(token) {
  return {
    type: Actions.CONFIRM_TOKEN,
    payload: token,
  }
}

export function setViewerData(data) {
  return {
    type: Actions.SET_VIEWER_DATA,
    payload: data,
  }
}

export function showModal(lastRepositoryId) {
  return {
    type: Actions.SHOW_MODAL,
    payload: lastRepositoryId,
  }
}

export function hideModal() {
  return {
    type: Actions.HIDE_MODAL,
  }
}