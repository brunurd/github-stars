import { combineReducers, createStore } from 'redux'
import notification from './reducers/notification'
import repositories from './reducers/repositories'
import search from './reducers/search'
import viewer from './reducers/viewer'
import user from './reducers/user'

const reducers = combineReducers({
  notification,
  repositories,
  search,
  viewer,
  user,
})

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && process.env.NODE_ENV != 'production' ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : undefined

export default createStore(reducers, devTools)
