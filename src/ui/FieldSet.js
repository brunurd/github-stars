import styled from 'styled-components'
import { em } from 'polished'
import { Icon } from './Icon.jsx'
import { Button } from './Button'

export const FieldSet = styled.fieldset`
  border: none;
  margin: 0;
  box-sizing: border-box;
  width: 100%;

  ${Button} {
    vertical-align: middle;

    :focus ${Icon} {
      stroke: ${({ theme }) => theme.colors.gray.light};
    }
  }

  input, ${Button}, ${Icon} {
    padding: 0;
  }

  input {
    border: none;
    font-size: ${em('16px')};
    width: calc(100% - 20px);

    :focus {
      outline: none;
    }
  }

  ${Icon} {
    fill: none;
    stroke: ${({ theme }) => theme.colors.gray.normal};
    transition: stroke 0.2s ease-out;

    :hover {
      stroke: ${({ theme }) => theme.colors.gray.light};
    }

    :active {
      stroke: ${({ theme }) => theme.colors.gray.dark};
    }
  }
`