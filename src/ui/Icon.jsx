import React from 'react'
import { PropTypes } from 'prop-types'
import styled from 'styled-components'

const Picture = styled.picture`
& *, & * * {
  fill: inherit;
  stroke: inherit;
}
`

const IconWrapper = ({ asset, className, width, height }) => {
  const { ReactComponent } = require(`../assets/${asset}`)

  return (
    <Picture className={className}>
      <ReactComponent width={width} height={height} viewBox={`0 0 ${width} ${height}`} />
    </Picture>
  )
}

IconWrapper.propTypes = {
  asset: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  className: PropTypes.string,
}

export const Icon = styled(IconWrapper)``