import styled from 'styled-components'
import { Icon } from './Icon.jsx'

export const IconTextContainer = styled.div`
  display: flex;
  flex-direction: row;
  box-sizing: border-box;
  vertical-align: text-bottom;

  ${Icon} {
    margin-right: 10px;
    vertical-align: inherit;
  }
`