import styled, { keyframes } from 'styled-components'
import { em } from 'polished'

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`

export const LoadingSpinnerStyle = styled.div`
  overflow: hidden;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  flex-grow: 2;
  font-family: ${({ theme }) => theme.fonts.main};

  img {
    animation: ${rotate} 2s linear infinite;
  }

  p {
    font-weight: 300;
    font-size: ${em('32px')};
    line-height: ${em('37px')};
    color: ${({ theme }) => theme.colors.gray.normal};
    margin: 0;
  }
`