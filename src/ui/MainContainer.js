import styled from 'styled-components'

export const MainContainer = styled.div`
  width: 90%;
  display: flex;
  flex-direction: row;
  box-sizing: border-box;
  align-items: flex-start;
  justify-content: center;
  margin: 46px auto;
`