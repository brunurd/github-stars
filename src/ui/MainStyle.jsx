import React, { useState, useEffect } from 'react'
import { PropTypes } from 'prop-types'
import styled, { createGlobalStyle } from 'styled-components'

const Global = createGlobalStyle`
  body {
    margin: 0;
  }
`

const Main = styled.main`
  height: ${({ height }) => height}px;
  display: flex;
  flex-direction: column;
`

const MainStyleWrapper = ({ className, children }) => {
  const [height, setHeight] = useState(window.innerHeight)
  const resize = () => setHeight(window.innerHeight)

  useEffect(() => {
    window.addEventListener('resize', resize)

    return () => {
      window.removeEventListener('resize', resize)
    }
  })

  return (
    <Main height={height} className={className}>
      <Global />
      {children}
    </Main>
  )
}

MainStyleWrapper.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

export const MainStyle = styled(MainStyleWrapper)``