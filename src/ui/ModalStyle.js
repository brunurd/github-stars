import styled from 'styled-components'
import { em } from 'polished'
import { Button } from './Button'

export const ModalStyle = styled.div`
  position: fixed;
  background-color: ${({ active }) => active ? 'rgba(0, 0, 0, 0.9)' : 'rgba(0, 0, 0, 0)' };
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  pointer-events: ${({ active }) => active ? 'auto' : 'none'};
  transition: background-color 0.4s ease-in;

  * {
    font-family: ${({ theme }) => theme.fonts.main};
    font-size: ${em('16px')};
    color: ${({ theme }) => theme.colors.gray.normal};
  }

  h1 {
    font-size: ${em('18px')};
  }

  form {
    width: 70%;
    max-width: 500px;
    background-color: white;
    box-sizing: border-box;
    padding: 32px;
    border-radius: 5px;
    box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
    display: flex;
    flex-direction: column;
    margin: auto;
    transform: ${({ active }) => active ? 'translate(0,0)' : 'translate(0, -600px)'};
    transition: transform 0.5s ease-out;

    & > * {
      margin-bottom: 16px;

      :last-child {
        margin-bottom: 0;
      }
    }

    input {
      border: none;
      max-width: 100%;
      height: 25px;
      padding: 10px;
      border-radius: 5px;
      box-shadow: inset 0 0 2.5px rgba(0, 0, 0, 0.4);
      transition: box-shadow 0.3s ease-out;

      :focus {
        outline: none;
      }
    }

    nav {
      display: flex;
      flex-direction: row;
      justify-content: flex-end;

      ${Button} {
        padding: 12px;
        width: 91px;
        border-width: 2px;
        border-style: solid;
        border-radius: 5px;
        border-color: ${({ theme }) => theme.colors.main.normal};
        background-color: white;
        color: ${({ theme }) => theme.colors.main.normal};
        margin-right: 16px;
        transition: 
          background-color 0.2s ease-in-out, 
          color 0.2s ease-in-out, 
          border-color 0.2s ease-in-out;

        :last-child {
          margin-right: 0;
        }

        :hover, :focus {
          border-color: ${({ theme }) => theme.colors.main.button};
          background-color: ${({ theme }) => theme.colors.main.button};
          color: white;
        }

        :active {
          border-color: ${({ theme }) => theme.colors.main.dark};
          background-color: ${({ theme }) => theme.colors.main.dark};
          color: white;
        }
      }
    }
  }
`