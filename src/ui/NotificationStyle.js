import styled from 'styled-components'
import { rgba, em } from 'polished'
import { Button } from './Button'

export const NotificationStyle = styled.div`
  display: flex;
  flex-direction: row;
  position: fixed;
  bottom: 32px;
  left: 32px;
  padding: 15px;
  box-sizing: border-box;
  border-radius: 5px;
  z-index: 2;
  transition: background-color 0.4s ease-in-out;
  font-family: ${({ theme }) => theme.fonts.main};
  pointer-events: ${({ active }) => active ? 'auto' : 'none'};
  background-color: ${({ active, theme }) => active ? theme.colors.main.dark : rgba(theme.colors.main.dark, 0)};

  p, ${Button} {
    font-size: ${em('16px')};
    line-height: ${em('19px')};
    transition: color 0.25s ease-out;
  }

  p {
    margin: 0;
    color: ${({ active }) => active ? 'rgba(255, 255, 255, 1)' : 'rgba(255, 255, 255, 0)'};
  }

  ${Button} {
    margin: 0 0 0 15px;
    color: ${({ active }) => active ? 'rgba(255, 255, 255, 0.4)' : 'rgba(255, 255, 255, 0)'};

    :hover, :focus {
      color: ${({ active }) => active ? 'rgba(255, 255, 255, 0.8)' : 'rgba(255, 255, 255, 0)'};
    }
  }
`