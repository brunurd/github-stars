import styled from 'styled-components'
import { em, lighten, darken } from 'polished'
import { IconTextContainer } from './IconTextContainer'
import { Button } from './Button'

export const RepoCardStyle = styled.div`
  width: 100%;
  padding: 27px 32px;
  background-color: white;
  border-radius: 5px;
  box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.15);
  width: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: center;
  vertical-align: middle;
  justify-content: space-between;
  color: ${({ theme }) => theme.colors.gray.normal};

  a {
    color: ${({ theme }) => theme.colors.gray.normal};
    text-decoration: none;

    :hover, :focus {
      color: ${({ theme }) => lighten(0.2, theme.colors.gray.normal)};
    }

    :active {
      color: ${({ theme }) => darken(0.2, theme.colors.gray.normal)};
    }
  }

  & > a {
    overflow: hidden;
    width: calc(100% - 91px);
  }

  ${IconTextContainer} {
    margin-top: 13px;
  }

  h3 {
    font-size: ${em('20px')};
    line-height: ${em('23px')};
    margin: 0 0 5px;
  }

  p {
    font-weight: 300;
    margin: 0;
  }

  p, ${Button} {
    font-size: ${em('16px')};
    line-height: ${em('19px')};
  }

  ${Button} {
    padding: 12px;
    width: 91px;
    margin-left: 32px;
    border-width: 2px;
    border-style: solid;
    border-radius: 5px;
    border-color: ${({ theme, starred }) => starred ? theme.colors.main.button : theme.colors.main.normal};
    background-color: ${({ theme, starred }) => starred ? theme.colors.main.button : 'white' };
    color: ${({ theme, starred }) => starred ? 'white' : theme.colors.main.normal};
    transition: 
      background-color 0.2s ease-in-out, 
      color 0.2s ease-in-out, 
      border-color 0.2s ease-in-out;

    :hover, :focus {
      border-color: ${({ theme, starred }) => starred ? theme.colors.main.light : theme.colors.main.light};
      background-color: ${({ theme, starred }) => starred ? theme.colors.main.light : 'white'};
      color: ${({ theme, starred }) => starred ? 'white' : theme.colors.main.light};
    }

    :active {
      border-color: ${({ theme }) => theme.colors.main.dark};
      background-color: ${({ theme }) => theme.colors.main.dark};
      color: white;
    }
  }
`