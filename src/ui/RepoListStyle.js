import styled from 'styled-components'
import { RepoCardStyle } from './RepoCardStyle'

export const RepoListStyle = styled.div`
  width: calc(100% - 232px);
  max-width: 902px;
  min-height: 700px;
  padding: 32px;
  border-radius: 5px;
  background-color: white;
  font-family: ${({ theme }) => theme.fonts.main};
  box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
  display: flex;
  flex-direction: column;
  align-items: center;
  box-sizing: border-box;
  fill: none;
  stroke: ${({ theme }) => theme.colors.main.normal};

  ${RepoCardStyle} {
    margin-bottom: 33px;

    :last-child {
      margin-bottom: 0;
    }
  }
`