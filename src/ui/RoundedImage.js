import styled from 'styled-components'

export const RoundedImage = styled.img`
  border-radius: 100%;
  width: 100%;
`