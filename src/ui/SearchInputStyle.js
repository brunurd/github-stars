import React from 'react'
import { PropTypes } from 'prop-types'
import styled from 'styled-components'
import { RoundedImage } from './RoundedImage'
import { FieldSet } from './FieldSet'

const CenterStyle = styled.div`
  box-sizing: border-box;
  justify-content: center;
  align-items: center;
  flex-wrap: nowrap;
  width: 100%;
  display: flex;
  height: 100%;
  max-width: 877px;
  flex-direction:  column;
  margin: auto;
  font-family: ${({ theme }) => theme.fonts.main};

  img {
    margin: 0 0 54px;
  }

  form {
    width: 100%;
  }

  ${FieldSet} {
    padding: 36px 34px;
    border-radius: 5px;
    box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.15);
    transition: box-shadow 0.3s ease-out;

    :focus-within {
      box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.3);
    }
  }

  ${RoundedImage} {
    display: none;
    height: 64px;
    width: 64px;
    margin-left: 32px;
  }

  @media screen and (max-width: 1102px) {
    margin: auto 19.58%;
    width: auto;
  }

  @media screen and (max-width: 535px) {
    flex-wrap: wrap;
  }
`

const TopStyle = styled.div`
  width: 100%;
  max-width: 1102px;
  display: flex;
  box-sizing: border-box;
  justify-content: center;
  align-items: center;
  flex-wrap: nowrap;
  flex-direction: row;
  margin: 0 auto;
  padding: 115px 0 46px 0;

  & > img {
    margin: 0;
  }

  form {
    width: 53.75%;
    margin-left: 46px;
  }

  ${RoundedImage} {
    height: 64px;
    width: 64px;
    margin-left: 32px;
    display: block;
  }

  ${FieldSet} {
    padding: 0 0 10px;
    border-bottom: 1px solid ${({ theme }) => theme.colors.gray.light};
  }

  @media screen and (max-width: 1102px) {
    width: auto;
    margin: 0 11.7361%;
  }

  @media screen and (max-width: 535px) {
    flex-wrap: wrap;

    ${RoundedImage} {
      margin-right: 0;
    }

    form {
      margin-left: 0;
    }
  }
`

const SearchInputStyleWrapper = props => {
  if (props.center) {
    return (
      <CenterStyle {...props} />
    )
  }

  return (
    <TopStyle {...props} />
  )
}

SearchInputStyleWrapper.propTypes = {
  center: PropTypes.bool,
}

export const SearchInputStyle = styled(SearchInputStyleWrapper)``