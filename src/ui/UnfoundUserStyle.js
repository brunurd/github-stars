import styled from 'styled-components'
import { em } from 'polished'

export const UnfoundUserStyle = styled.div`
  overflow: hidden;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  flex-grow: 2;
  font-family: ${({ theme }) => theme.fonts.main};

  p {
    font-weight: 300;
    font-size: ${em('32px')};
    line-height: ${em('37px')};
    color: ${({ theme }) => theme.colors.gray.normal};
    margin: 0;
  }
`