import styled from 'styled-components'
import { em } from 'polished'
import { RoundedImage } from './RoundedImage'

export const UserBarCard = styled.div`
  box-sizing: border-box;
  background-color: rgba(0, 0, 0, 0.09);
  padding: 27px 37px;

  ${RoundedImage} {
    margin-bottom: 18px;
  }

  h1, h2 {
    font-size: ${em('20px')};
    line-height: ${em('23px')};
    text-align: center;
    margin: 0;
  }

  h1 {
    font-weight: normal;
  }

  h2 {
    font-weight: 300;
  }
`