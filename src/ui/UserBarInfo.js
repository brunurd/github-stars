import styled from 'styled-components'
import { IconTextContainer } from './IconTextContainer'

export const UserBarInfo = styled.div`
  padding: 32px 22px;
  box-sizing: border-box;

  & > * {
    width: 100%;
    text-align: left;
    box-sizing: border-box;
    margin: 0 0 16px;

    :last-child {
      margin-bottom: 0;
    }
  }

  & > p {
    margin: 0 0 32px;
    max-width: 232px;
  }

  & > ${IconTextContainer} {
    justify-content: left;

    p {
      margin: 0;
    }
  }
`