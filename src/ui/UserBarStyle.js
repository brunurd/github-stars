import styled from 'styled-components'
import { em, darken, lighten } from 'polished'

export const UserBarStyle = styled.aside`
  width: 232px;
  background-color: ${({ theme }) => theme.colors.main.normal};
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  box-sizing: border-box;
  box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
  color: white;
  font-family: ${({ theme }) => theme.fonts.main};
  font-weight: normal;
  font-size: ${em('14px')};
  fill: none;
  stroke: ${({ theme }) => theme.colors.main.light};
  margin-top: 32px;

  a {
    text-decoration: none;
    color: white;
    transition: color 0.2s ease-out;

    :focus {
      color: ${darken(0.1, '#fff')};
      outline: none;
    }

    :hover {
      color: ${({ theme }) => lighten(0.1, theme.colors.main.light)};
    }

    :active {
      color:  ${({ theme }) => darken(0.2, theme.colors.main.light)};
    }
  }
`