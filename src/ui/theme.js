export default {
  colors: {
    main: {
      light: '#B1B1E6',
      button: '#5152B7',
      normal: '#5253B9',
      dark: '#23245C',
    },
    gray: {
      light: '#BABABA',
      normal: '#4F4F4F',
      dark: '#2D2D2D',
    },
  },
  fonts: {
    main: '"Roboto", sans-serif',
  },
}