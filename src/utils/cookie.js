export function setCookie(name, value, daysToExpire = 1) {
  const date = new Date()
  date.setTime(date.getTime() + (daysToExpire * 24 * 60 * 60 * 1000))
  var expires = `expires=${date.toUTCString()}`
  document.cookie = `${name}=${JSON.stringify(value)};${expires};path=/`
}

export function getCookie(name) {
  const cookieName = `${name}=`
  const cookies = document.cookie.split(';')

  for(let cookie of cookies) {
    while (cookie.charAt(0) == ' ') {
      cookie = cookie.substring(1)
    }

    if (cookie.indexOf(cookieName) == 0) {
      const value = cookie.substring(cookieName.length, cookie.length)
      return JSON.parse(value)
    }
  }

  return ''
}