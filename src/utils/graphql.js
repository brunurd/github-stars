import request from './request'

const appToken = 'bff56d41d7f8c325635496a82d1d121c4715e157'

export function graphql(body, callback, token) {
  request('https://api.github.com/graphql', 'POST', {
    'content-type': 'application/json',
    'Authorization': `Bearer ${token || appToken}`,
  }, { query: body }, callback)
}

export function getUserData(username, callback) {
  graphql(`
  {
    search(query: "${username}", type: USER, first: 1) {
      edges {
        node {
          ... on User {
            login
            name
            bio
            location
            company
            email
            websiteUrl
            avatarUrl
          }
        }
      }
    }
  }
  `, callback)
}

export function getRepositories(username, count, callback, lastCursor, token = appToken) {
  graphql(`
  {
    user(login: "${username}") {
      starredRepositories (first: ${count}${lastCursor ? `, after: "${lastCursor}"` : ''}) {
        edges {
          cursor
          node {
            id
            url
            nameWithOwner
            description
            ${token && token !== appToken ? 'viewerHasStarred' : ''}
            stargazers {
              totalCount
            }
          }
        }
      }
    }
  }
  `, callback, token)
}

export function getViewerData(token, callback) {
  graphql(`
  {
    viewer {
      login
      avatarUrl
    }
  }
  `, callback, token)
}

export function addStar(token, login, repoId, callback) {
  graphql(`
  mutation {
    addStar(input: {clientMutationId: "${login}", starrableId: "${repoId}"}) {
      clientMutationId
    }
  }
  `, callback, token)
}

export function removeStar(token, login, repoId, callback) {
  graphql(`
  mutation {
    removeStar(input: {clientMutationId: "${login}", starrableId: "${repoId}"}) {
      clientMutationId
    }
  }
  `, callback, token)
}