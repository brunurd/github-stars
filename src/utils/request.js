function formatError(...errors) {
  return {
    data: {},
    errors,
  }
}

function resolveResponse(response, callback) {
  response.json()
    .then(json => {
      if (response.status !== 200) {
        if (callback) {
          callback.bind(null, formatError(json))()
        }
        return
      }
      if (callback) {
        callback.bind(null, json)()
      }
    })
}

export default function request(url, method, headers, body, callback) {
  fetch(url, {
    method,
    headers,
    body: JSON.stringify(body),
  })
    .then(response => resolveResponse(response, callback))
    .catch(err => {
      if (callback) {
        callback.bind(null, formatError(err))()
      }
    })
}