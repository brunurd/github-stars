import * as searchReducer from '../store/reducers/search'
import { updateUser } from '../store/reducers/user'
import { reset } from '../store/reducers/repositories'
import store from '../store/store'
import { getUserData } from './graphql'
import updateStarredRepositories from './updateStarredRepositories'

export default function search(sentence, token) {
  store.dispatch(searchReducer.searchSentence(sentence))
  getUserData(sentence, result => {
    if (result.errors) {
      store.dispatch(searchReducer.setSearchState(searchReducer.SearchStates.UNFOUND))
      return
    }

    // Check if data is empty.
    if (!result.data.search) {
      store.dispatch(searchReducer.setSearchState(searchReducer.SearchStates.UNFOUND))
      return
    }

    // check if found something.
    if (result.data.search.edges.length == 0) {
      store.dispatch(searchReducer.setSearchState(searchReducer.SearchStates.UNFOUND))
      return
    }

    const user = result.data.search.edges[0].node

    // check if match.
    if (user.name !== sentence && user.login !== sentence) {
      store.dispatch(searchReducer.setSearchState(searchReducer.SearchStates.UNFOUND))
      return
    }

    store.dispatch(updateUser(user))
    store.dispatch(reset())

    updateStarredRepositories(user.login, () => {
      store.dispatch(searchReducer.setSearchState(searchReducer.SearchStates.FOUND))
    }, null, token)
  })
}