import { getRepositories } from './graphql'
import store from '../store/store'
import { addRepository, setLastCursor } from '../store/reducers/repositories'

export default function updateStarredRepositories(username, callback, lastCursor, token) {
  getRepositories(username, 4, result => {
    if (!result.errors) {
      if (result.data.user) {
        if (result.data.user.starredRepositories.edges.length > 0) {
          for (let edge of result.data.user.starredRepositories.edges) {
            store.dispatch(setLastCursor(edge.cursor))
            store.dispatch(addRepository(edge.node))
          }
        }
      }
    }

    if (callback) {
      callback()
    }
  }, lastCursor, token)
}