import { getViewerData } from './graphql'
import { setViewerData } from '../store/reducers/viewer'
import { confirmToken } from '../store/reducers/viewer'
import store from '../store/store'
import search from './search'

export default function updateViewerData(token, username) {
  getViewerData(token, result => {
    if (result.errors) {
      return
    }

    if (!result.data.viewer) {
      return
    }

    store.dispatch(confirmToken(token))
    store.dispatch(setViewerData(result.data.viewer))

    if (username && token) {
      search(username, token)
    }
  })
}