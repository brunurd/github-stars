const { join, resolve } = require('path')
const CopyPlugin = require('copy-webpack-plugin')
const HtmlWebPackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    app: './src/index.js',
  },
  devtool: '#eval-source-map',
  devServer: {
    contentBase: join(__dirname, 'build'),
    compress: true,
    port: 9000,
    historyApiFallback: true,
  },
  output: {
    filename: '[name].js',
    path: resolve(__dirname, 'build'),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', 'url-loader'],
      },
      {
        test: /\.(png|jpeg|jpg|gif|jpg2|jpf)$/,
        use: 'url-loader',
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: resolve(__dirname, 'public', 'index.html'),
      filename: './index.html',
    }),
    new CopyPlugin([
      { from: '**/*', context: 'public', ignore: 'index.html' },
    ]),
  ],
}